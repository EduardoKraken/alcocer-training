import Vue from 'vue'
import VueRouter from 'vue-router'
import HomeView  from '../views/HomeView.vue'
import Login     from '../views/Login.vue'

// CATALOGOS
import Categorias     from '../views/catalogos/Categorias.vue'
import Ejercicios     from '../views/catalogos/Ejercicios.vue'
import Usuarios       from '../views/catalogos/Usuarios.vue'

Vue.use(VueRouter)

const routes = [
  { path: '/'          , name: 'Login'      , component: Login },
  { path: '/home'      , name: 'Home'       , component: HomeView },
  { path: '/categorias', name: 'Categorias' , component: Categorias },
  { path: '/ejercicios', name: 'Ejercicios' , component: Ejercicios },
  { path: '/usuarios'  , name: 'Usuarios'   , component: Usuarios },
]

const router = new VueRouter({
  routes
})

export default router
