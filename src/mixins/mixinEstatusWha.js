import Vue from 'vue'
import store from '@/store'
import {mapGetters, mapActions}    from 'vuex'

import { io }         from "socket.io-client";

var QRCode = require('qrcode')

export default {
	data () {
		return {
			socket:null,
      mensajeProceso:'',
		}
	},
	computed: {
		...mapGetters('login',[ 'getdatosUsuario' ]),
	},

	mounted() {

    if( this.getdatosUsuario.url_servidor  ){
      const url = this.getdatosUsuario.url_servidor.replace('kpi/', '')
      console.log(url)

        // Modo servidor

        const path = '/ws/socket.io'
        
        this.socket = io(url, {
          transports: ["websocket"], path
        });

        // Modo local
        // this.socket = io(`ws://localhost:3008/`, {
        //   transports: ["websocket"], path
        // });

        this.socket.on("connect", ( ) => {
          // Conectar cocket
          console.log('estoy conectado al socket, enserio')

          // Mandar a decirle quién se conecto
          this.socket.emit('iniciarSesionWha', this.getdatosUsuario )
        });

        this.socket.on("disconnect", () => {
          console.log('Desconectado del socket');
        });

        // Generar el QR de la recepcionista
        this.socket.on('QR-generado', ( body ) => {

        	if( body.from == this.getdatosUsuario.whatsappservidor ){
  	      	// Generamos el QR y lo agregamos al canvas
  	        QRCode.toCanvas(document.getElementById('canvas'), body.qr  , (error) => {
  	          if (error) console.error(error)
  	          console.log('success!');
  	        	// Dejamos de cargar
  	          this.cargar = false
  	        })
        	}

        })

        // Indicamos si la session esta activa o no
        this.socket.on('session-activa', ( body ) => {

        	if( this.getdatosUsuario.whatsappservidor == body  ){
          	this.estatusWhats = 1
            if( this.estatusWhats == 1 ){ this.cargar = false }
        	}
        
        })


        // Indicamos si la session esta activa o no
        this.socket.on('estatus-whatsapp', ( body ) => {

          if( this.getdatosUsuario.whatsappservidor == body.from  ){
            const canvasElement = document.getElementById('canvas')
            const context = canvasElement.getContext('2d');
            // Limpiar el contenido del canvas
            context.clearRect(0, 0, canvasElement.width, canvasElement.height);

            this.mensajeProceso = body.mensaje
            this.estatusWhats   = body.estatus ? body.estatus : 0

            if( this.estatusWhats == 1 ){ this.cargar = false }
          }
        
        })

        // Validamos que la sesión este inactiva
        this.socket.on('session-inactiva', ( body ) => {
        	if( this.getdatosUsuario.whatsappservidor == body  ){
            const canvasElement = document.getElementById('canvas')
            const context = canvasElement.getContext('2d');
            // Limpiar el contenido del canvas
            context.clearRect(0, 0, canvasElement.width, canvasElement.height);

          	this.estatusWhats = 2
        	}
          
        })
      }
    },

    methods: {
      validarEstatus () {
        this.mensajeProceso = ''

      	// Validamos la información del usuario y que tenga su WhatsAppservidor
      	if( !this.getdatosUsuario.whatsappservidor ){ return this.validarErrorDirecto('FAVOR DE INGRESAR UN TELÉFONO') }

      	// Cargamos y abrimos un cuadro de dialogo 
        this.cargar = true

      	// Creamos el payload que vamos a enviar
        const payload = {
        	from: this.getdatosUsuario.whatsappservidor
        }

        // Genero la ruta oficial para hacer la petición
        this.url_servidor = this.getdatosUsuario.url_servidor + 'whatsapp.activo'

        // Hago la petición
        return this.$http.post(this.url_servidor, payload).then(response=>{
        	this.cargar = false
        	this.dialog = true
        }).catch( error =>{
        	this.estatusWhats   = 3
        	this.dialog         = true
          this.validarError( error )
        }).finally( () => { this.cargar = false })
      },

      // Función para escanear el código QR
			escanerQR () {

      	// Validamos la información del usuario y que tenga su WhatsAppservidor
      	if( !this.getdatosUsuario.whatsappservidor ){ return this.validarErrorDirecto('FAVOR DE INGRESAR UN TELÉFONO') }

      	// Formamos el payload para cargar el usuario
        const payload = {
        	from: this.getdatosUsuario.whatsappservidor
        }
        
        this.cargar = true
      	this.dialog = false

        // Genero la ruta oficial para hacer la petición

        const path = 'whatsapp.inicializar'
        this.url_servidor = this.getdatosUsuario.url_servidor + path

        // Realizo la petición
        return this.$http.post(this.url_servidor, payload).then(response=>{
        	this.dialog = true
        }).catch( error =>{
        	this.dialog = true
          this.validarError( error )
        })
      },

      // Detruir la sesión de wpp por si hay algún error en todo esto
			cerrarSesion () {
      	// Validamos la información del usuario y que tenga su WhatsAppservidor
      	if( !this.getdatosUsuario.whatsappservidor ){ return this.validarErrorDirecto('FAVOR DE INGRESAR UN TELÉFONO') }

        this.cargar = true
      	this.dialog = false

      	// Formamos el payload para cargar el usuario
        const payload = {
        	from: this.getdatosUsuario.whatsappservidor
        }

        // Genero la ruta oficial para hacer la petición
        this.url_servidor = this.getdatosUsuario.url_servidor + 'whatsapp.destruir'
        
        // Realizo la petición
        return this.$http.post(this.url_servidor, payload).then(response=>{
        	// Indico que ya no hay una sessión
        	this.estatusWhats = 2
        	this.cargar = false
        	this.dialog = true
        }).catch( error =>{
        	this.dialog = true
          this.validarError( error )
        }).finally( () => { this.cargar = false })
      },


    },
}