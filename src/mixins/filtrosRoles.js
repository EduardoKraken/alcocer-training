import Vue from 'vue'
import { mapGetters,mapActions } from 'vuex';

import XLSX from 'xlsx'

export default {
	data: () => ({
    headersBloque1: [
      { text: 'Grupo'        , value: 'grupo'       },
      { text: 'Inicio'       , value: 'hora_inicio' },
      { text: 'Final'        , value: 'hora_fin'    },
      { text: '% asist.'     , value: 'porcentaje_asistencia' },
      { text: 'Alumnos'      , value: 'cant_alumnos'},
      { text: 'Nivel'        , value: 'id_nivel'    },
      { text: 'Sucursal'     , value: 'plantel'     },
      { text: 'Modalidad'    , value: 'modalidad'   },
      { text: 'Lunes 1'      , value: 'lu1_n'       },
      { text: 'Martes 1'     , value: 'ma1_n'       },
      { text: 'Miércoles 1'  , value: 'mi1_n'       },
      { text: 'Jueves 1'     , value: 'ju1_n'       },
      { text: 'Viernes 1'    , value: 'vi1_n'       },
    ],

    headersBloque2: [
      { text: 'Grupo'        , value: 'grupo'       },
      { text: 'Inicio'       , value: 'hora_inicio' },
      { text: 'Final'        , value: 'hora_fin'    },
      { text: '% asist.'     , value: 'porcentaje_asistencia' },
      { text: 'Alumnos'      , value: 'cant_alumnos'},
      { text: 'Nivel'        , value: 'id_nivel'    },
      { text: 'Sucursal'     , value: 'plantel'     },
      { text: 'Modalidad'    , value: 'modalidad'   },
      { text: 'Lunes 2'      , value: 'lu2_n'       },
      { text: 'Martes 2'     , value: 'ma2_n'       },
      { text: 'Miércoles 2'  , value: 'mi2_n'       },
      { text: 'Jueves 2'     , value: 'ju2_n'       },
      { text: 'Viernes 2'    , value: 'vi2_n'       },
    ],

    headersBloque3: [
      { text: 'Grupo'        , value: 'grupo'       },
      { text: 'Inicio'       , value: 'hora_inicio' },
      { text: 'Final'        , value: 'hora_fin'    },
      { text: '% asist.'     , value: 'porcentaje_asistencia' },
      { text: 'Alumnos'      , value: 'cant_alumnos'},
      { text: 'Nivel'        , value: 'id_nivel'    },
      { text: 'Sucursal'     , value: 'plantel'     },
      { text: 'Modalidad'    , value: 'modalidad'   },
      { text: 'Lunes 3'      , value: 'lu3_n'       },
      { text: 'Martes 3'     , value: 'ma3_n'       },
      { text: 'Miércoles 3'  , value: 'mi3_n'       },
      { text: 'Jueves 3'     , value: 'ju3_n'       },
      { text: 'Viernes 3'    , value: 'vi3_n'       },
    ],

    headersBloque4: [
      { text: 'Grupo'        , value: 'grupo'       },
      { text: 'Inicio'       , value: 'hora_inicio' },
      { text: 'Final'        , value: 'hora_fin'    },
      { text: '% asist.'     , value: 'porcentaje_asistencia' },
      { text: 'Alumnos'      , value: 'cant_alumnos'},
      { text: 'Nivel'        , value: 'id_nivel'    },
      { text: 'Sucursal'     , value: 'plantel'     },
      { text: 'Modalidad'    , value: 'modalidad'   },
      { text: 'Lunes 4'      , value: 'lu4_n'       },
      { text: 'Martes 4'     , value: 'ma4_n'       },
      { text: 'Miércoles 4'  , value: 'mi4_n'       },
      { text: 'Jueves 4'     , value: 'ju4_n'       },
      { text: 'Viernes 4'    , value: 'vi4_n'       },
    ],

    headers:[],
    selectedHeaders: 1,

    checkEliminados: false,
    checkOptimizados: 3,
    curso: null,
  }),

  created(){
  	this.headers = this.headersBloque1
  },

  watch: {
  	selectedHeaders( val ){
  		switch( val ){
  			case 1: 
  				this.headers = this.headersBloque1
  			break;

  			case 2: 
  				this.headers = this.headersBloque2
  			break;

  			case 3: 
  				this.headers = this.headersBloque3
  			break;

  			case 4: 
  				this.headers = this.headersBloque4
  			break;
  		}
  	}
  },

  computed:{
  	filterGrupos(){

  		let grupos = []

  		if( this.checkEliminados ){
  			grupos = this.gruposRoles.filter( el => { return el.deleted == 1 })
  		}else{
  			grupos = this.gruposRoles.filter( el => { return el.deleted == 0 })
  		}
      
  		grupos = grupos.filter( el => { return el.lu1 == this.getdatosUsuario.iderp || el.ma1 == this.getdatosUsuario.iderp || el.mi1 == this.getdatosUsuario.iderp || el.ju1 == this.getdatosUsuario.iderp || el.vi1 == this.getdatosUsuario.iderp || el.lu2 == this.getdatosUsuario.iderp || el.ma2 == this.getdatosUsuario.iderp || el.mi2 == this.getdatosUsuario.iderp || el.ju2 == this.getdatosUsuario.iderp || el.vi2 == this.getdatosUsuario.iderp || el.lu3 == this.getdatosUsuario.iderp || el.ma3 == this.getdatosUsuario.iderp || el.mi3 == this.getdatosUsuario.iderp || el.ju3 == this.getdatosUsuario.iderp || el.vi3 == this.getdatosUsuario.iderp || el.lu4 == this.getdatosUsuario.iderp || el.ma4 == this.getdatosUsuario.iderp || el.mi4 == this.getdatosUsuario.iderp || el.ju4 == this.getdatosUsuario.iderp || el.vi4  == this.getdatosUsuario.iderp })

  		if( this.nivel ){
  			grupos = grupos.filter( el => { return el.id_nivel == this.nivel })
  		}

  		if( this.plantel ){
  			grupos = grupos.filter( el => { return el.plantel == this.plantel })
  		}

  		if( this.horario ){
  			grupos = grupos.filter( el => { return el.hora_inicio == this.horario })
  		}

  		if( [0,1].includes(this.modalidad)){
  			grupos = grupos.filter( el => { return el.online == this.modalidad })
  		}

      if( this.curso ){
        grupos = grupos.filter( el => { return el.curso == this.curso })
      }
  		

  		return grupos
  	},

    gruposCompletos( ){
      return this.filterGrupos.filter( el => { return el.lu1_n && el.ju1_n  })
    },

    gruposFaltantes( ){
      return this.filterGrupos.filter( el => { return !el.lu1_n || !el.ju1_n  })
    },

    horasSemana( ){
      return this.filterGrupos.map(item => item.horas_ciclo ).reduce((prev, curr) => prev + curr, 0)
    },

    horasCatorcena( ){
      return this.filterGrupos.map(item => item.horas_catorcena ).reduce((prev, curr) => prev + curr, 0)
    },

    horasCiclo( ){
      return this.filterGrupos.map(item => item.horas_semana ).reduce((prev, curr) => prev + curr, 0)
    }

  },

	methods: {

    exportExcel(dataExport, name){
      let data = XLSX.utils.json_to_sheet(dataExport)
      const workbook = XLSX.utils.book_new()
      const filename = name
      XLSX.utils.book_append_sheet(workbook, data, filename)

      var wbout = XLSX.write(workbook, {
        bookType: 'csv',
        bookSST: false,
        type: 'binary'
      });

      const file = new File([new Blob([this.s2ab(wbout)])], filename + '.csv')
      
      let formData = new FormData();
      // //se crea el objeto y se le agrega como un apendice el archivo 
      formData.append('file',file);
      // /*getDatosRiesgo the form data*/

      this.$http.post('pdfs',formData).then(response=> {
        window.location = this.$http.options.root + 'pdfs/' + filename + '.csv'
      }).catch(error=> {
        console.log(error);
      });
    },

    s2ab(s) {
      var buf = new ArrayBuffer(s.length);
      var view = new Uint8Array(buf);
      for (var i = 0; i != s.length; ++i) view[i] = s.charCodeAt(i) & 0xFF;
      return buf;
    },

  }
}